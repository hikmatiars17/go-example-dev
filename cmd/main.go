package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"

	"google.golang.org/grpc"

	"github.com/budhip/common/postgre"
	"github.com/gofiber/fiber/v2"

	commonGrpc "github.com/budhip/common/grpc"
	cfg "github.com/budhip/env-config"
	viperConfig "github.com/budhip/env-config/viper"

	articleClient "bitbucket.org/Amartha/example-article/client"
	conf "bitbucket.org/Amartha/example-user/config"
	deliveryGrpc "bitbucket.org/Amartha/example-user/delivery/grpc"
	handler "bitbucket.org/Amartha/example-user/delivery/http"
	postgreUserRepo "bitbucket.org/Amartha/example-user/repository"
	userSrv "bitbucket.org/Amartha/example-user/service"
)

func getConfig() (cfg.Config, error) {
	return viperConfig.NewConfig("amarthacore", "config.json")
}

func main() {
	sugarLogger := conf.InitLogger()

	defer sugarLogger.Sync()


	config, err := getConfig()
	if err != nil {
		sugarLogger.Errorf("can not read config.json: %s", err)
		return
	}

	dbHost := config.GetString(`database.host`)
	dbPort := config.GetString(`database.port`)
	dbUser := config.GetString(`database.user`)
	dbPass := config.GetString(`database.pass`)
	dbName := config.GetString(`database.name`)

	dbConfig := postgre.Config{
		Host:     dbHost,
		Port:     dbPort,
		User:     dbUser,
		Password: dbPass,
		Name:     dbName,
	}

	db, err := postgre.DB(dbConfig)
	if err != nil {
		sugarLogger.Errorf("failed connect to database: %s", err)
		return
	}
	defer db.Close()

	// Creates a new Fiber instance.
	app := fiber.New(fiber.Config{
		AppName:      "Fiber Example User Clean Architecture",
		ServerHeader: "Example User",
	})

	grpcAddr := config.GetString("server.user_address")
	httpAddr := config.GetString("server.user_address_http")

	snow := conf.NewSnowflake()

	// service
	upr := postgreUserRepo.NewPostgreRepository(db)

	// connect to article service
	articleClientAddr := config.GetString(`server.article_address`)
	aClient, errC := articleClient.NewArticleClient(articleClientAddr)
	if errC != nil {
		sugarLogger.Errorf("err connect to Article Service: %s", err)
	}

	userService := userSrv.NewUserService(upr, aClient, snow)

	api := app.Group("/api")
	// Prepare our endpoints for the API.
	handler.NewUserHandler(api.Group("/v1/users"), userService)

	// Prepare an endpoint for 'Not Found'.
	app.All("*", func(c *fiber.Ctx) error {
		errorMessage := fmt.Sprintf("Route '%s' does not exist in this API!", c.OriginalURL())

		return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
			"status":  "fail",
			"message": errorMessage,
		})
	})

	// grpc
	pbServer := grpc.NewServer(commonGrpc.WithDefault()...)
	deliveryGrpc.NewUserServerGRPC(pbServer, userService)
	go func() {
		commonGrpc.Serve(grpcAddr, pbServer)
	}()

	go func() {
		sugarLogger.Fatal(app.Listen(httpAddr))
	}()

	sugarLogger.Infof("gRPC server started. Listening on port: %s", grpcAddr)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	sugarLogger.Infof("All server stopped!")
}
