package service

import (
	"context"
	"time"

	"bitbucket.org/Amartha/example-user/model"
	"bitbucket.org/Amartha/example-user/repository"

	aClient "bitbucket.org/Amartha/example-article/client"
	conf "bitbucket.org/Amartha/example-user/config"
)

type UserService interface {
	CreateUser(ctx context.Context, user *model.AddNewUserRequest) (*model.User, error)
	GetUserByID(ctx context.Context, id int64) (*model.User, error)
	GetUserArticle(ctx context.Context, id int64) (*model.UserArticle, error)
	UpdateArticleViewedByEmail(ctx context.Context, email string) error
}

type userService struct {
	userRepo      repository.PostgreUserRepository
	articleClient aClient.ArticleClient
	snowFlake     conf.Snowflake
}

// NewUserService will create new an userService object representation of UserService interface
func NewUserService(m repository.PostgreUserRepository, a aClient.ArticleClient, sf conf.Snowflake) UserService {
	return &userService{m, a, sf}
}

func (us *userService) CreateUser(ctx context.Context, u *model.AddNewUserRequest) (*model.User, error) {
	idSnow := us.snowFlake.GetNewID()

	userID := idSnow
	createdAt := time.Now().Format("2006-01-02 15:04:5")
	updatedAt := time.Now().Format("2006-01-02 15:04:5")

	userModel := &model.User{
		ID:        userID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}

	err := us.userRepo.StoreUser(userModel)
	if err != nil {
		return nil, err
	}

	return userModel, nil
}

func (us *userService) GetUserByID(ctx context.Context, id int64) (*model.User, error) {
	//ctx2 := context.Background()
	//conf.WithContext(ctx).Info("START GetUserByID")

	logger := conf.Logger(ctx)

	logger.Info("START GetUserByID")

	list, err := us.userRepo.GetUserByID(id)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (us *userService) GetUserArticle(ctx context.Context, id int64) (*model.UserArticle, error) {
	list, err := us.userRepo.GetUserByID(id)
	if err != nil {
		return nil, err
	}

	getArticlesByUserID, err := us.articleClient.GetAllArticleByUserID(ctx, list.ID)
	if err != nil {
		return nil, err
	}

	articleLists := make([]*model.Article, 0)
	for i := 0; i < len(getArticlesByUserID.Articles); i++ {
		articleList := &model.Article{
			ID:        getArticlesByUserID.Articles[i].Id,
			Title:     getArticlesByUserID.Articles[i].Title,
			Content:   getArticlesByUserID.Articles[i].Content,
			CreatedAt: getArticlesByUserID.Articles[i].CreatedAt,
			UpdatedAt: getArticlesByUserID.Articles[i].UpdatedAt,
		}

		articleLists = append(articleLists, articleList)
	}

	return &model.UserArticle{
		ID:        list.ID,
		FirstName: list.FirstName,
		LastName:  list.LastName,
		Email:     list.Email,
		CreatedAt: list.CreatedAt,
		UpdatedAt: list.UpdatedAt,
		Articles:  articleLists,
	}, nil
}

func (us *userService) UpdateArticleViewedByEmail(ctx context.Context, email string) error {
	updatedAt := time.Now().Format("2006-01-02 15:04:5")

	err := us.userRepo.UpdateArticleViewedByEmail(email, updatedAt)
	if err != nil {
		return err
	}

	return nil
}
