-- +goose Up
-- +goose StatementBegin
CREATE TABLE "user" (
    id bigint NOT NULL,
    first_name varchar(45) NOT NULL,
    last_name varchar(45) NOT NULL,
    email varchar(255)  NOT NULL,
    updated_at timestamp DEFAULT NULL,
    created_at timestamp DEFAULT NULL,
    PRIMARY KEY (id));
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE "user";
-- +goose StatementEnd
