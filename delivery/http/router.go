package http

import (
	"context"
	"log"
	"net/http"
	"regexp"
	"strings"

	userSrv "bitbucket.org/Amartha/example-user/service"
	cfg "github.com/budhip/env-config"
)

type restHandlerService struct {
	userSrv userSrv.UserService
	config  cfg.Config
}

type route struct {
	method  string
	regex   *regexp.Regexp
	handler http.HandlerFunc
}

type ctxKey struct{}

var routes = []route{}

func NewRestHandlerService(c userSrv.UserService, v cfg.Config) RestHandlerService {
	return &restHandlerService{c, v}
}

type RestHandlerService interface {
	Serve(w http.ResponseWriter, r *http.Request)
}

func Route(method, pattern string, handler http.HandlerFunc) {
	routes = append(routes, route{method, regexp.MustCompile("^" + pattern + "$"), handler})
}

// func httpResponseWrite(rw http.ResponseWriter, response interface{}, statusCode int) {
//	rw.Header().Set("Content-type", "application/json")
//	rw.WriteHeader(statusCode)
//	err := json.NewEncoder(rw).Encode(response)
//	if err != nil {
//		logger.Error(err)
//	}
//}

func (t *restHandlerService) Serve(w http.ResponseWriter, r *http.Request) {
	var allow []string
	for _, route := range routes {
		matches := route.regex.FindStringSubmatch(r.URL.Path)
		if len(matches) > 0 {
			if r.Method != route.method {
				allow = append(allow, route.method)
				continue
			}
			ctx := context.WithValue(r.Context(), ctxKey{}, matches[1:])
			route.handler(w, r.WithContext(ctx))
			return
		}
	}
	if len(allow) > 0 {
		w.Header().Set("Allow", strings.Join(allow, ", "))
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.NotFound(w, r)
}

func Param(r *http.Request, index int) string {
	params, ok := r.Context().Value(ctxKey{}).([]string)
	if !ok {
		log.Println(" Invalid ")
	}
	return params[index]
}
