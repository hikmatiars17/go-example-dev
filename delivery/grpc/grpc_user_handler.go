package grpc

import (
	"context"

	"bitbucket.org/Amartha/example-user/model"

	protoc "bitbucket.org/Amartha/example-user/delivery/pb"
)

func (s *server) CreateUser(ctx context.Context,
	request *protoc.CreateUserRequest) (*protoc.CreateUserResponse, error) {
	reqIns := &model.AddNewUserRequest{
		FirstName: request.FirstName,
		LastName:  request.LastName,
		Email:     request.Email,
	}
	resp, err := s.service.CreateUser(ctx, reqIns)
	if err != nil {
		return nil, err
	}

	return &protoc.CreateUserResponse{
		Id:        resp.ID,
		FirstName: resp.FirstName,
		LastName:  resp.LastName,
		Email:     resp.Email,
		CreatedAt: resp.CreatedAt,
		UpdatedAt: resp.UpdatedAt,
	}, nil
}

func (s *server) GetUserByID(ctx context.Context, req *protoc.GetUserByIDRequest) (*protoc.CreateUserResponse, error) {
	resp, err := s.service.GetUserByID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	return &protoc.CreateUserResponse{
		Id:        resp.ID,
		FirstName: resp.FirstName,
		LastName:  resp.LastName,
		Email:     resp.Email,
		CreatedAt: resp.CreatedAt,
		UpdatedAt: resp.UpdatedAt,
	}, nil
}

func (s *server) GetUserArticle(ctx context.Context,
	req *protoc.GetUserByIDRequest) (*protoc.UserArticleResponse, error) {
	resp, err := s.service.GetUserArticle(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	var list *protoc.Article
	lists := make([]*protoc.Article, 0)
	for i := 0; i < len(resp.Articles); i++ {
		list = &protoc.Article{
			Id:        resp.Articles[i].ID,
			Title:     resp.Articles[i].Title,
			Content:   resp.Articles[i].Content,
			CreatedAt: resp.Articles[i].CreatedAt,
			UpdatedAt: resp.Articles[i].UpdatedAt,
		}
		lists = append(lists, list)
	}

	return &protoc.UserArticleResponse{
		Id:        resp.ID,
		FirstName: resp.FirstName,
		LastName:  resp.LastName,
		Email:     resp.Email,
		CreatedAt: resp.CreatedAt,
		UpdatedAt: resp.UpdatedAt,
		Articles:  lists,
	}, nil
}
